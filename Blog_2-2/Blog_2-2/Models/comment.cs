﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2_2.Models
{
    public class Comment
    {
        public int ID { set; get; }
        
        [Required(ErrorMessage = "Bạn không được bỏ trống")]
        [MinLength(50, ErrorMessage = "Bạn phải nhập ít nhất 50 kí tự")]
        public String Body { set; get; }
        
        [Required(ErrorMessage = "Bạn không được bỏ trống")]
        [DataType(DataType.DateTime, ErrorMessage ="Bạn nhập ngày không đúng")]
        public DateTime DataCreated { set; get; }
         
        [Required(ErrorMessage = "Bạn không được bỏ trống")]
        [DataType(DataType.DateTime, ErrorMessage = "Bạn nhập ngày không đúng")]
        public DateTime DateUpdated { set; get;  }
        
        [Required(ErrorMessage = "Bạn không được bỏ trống")]
        public String Author { set; get; }

        public int PostID { set; get; }

        public virtual Post Post { set; get; }
    }
}