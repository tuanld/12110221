﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog_2_2.Models
{
    [Table("Bai Viet")]
    public class Post
    {
        [Range(2,100)]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public int ID { set; get; }
        
        [Required(ErrorMessage = "Bạn không được bỏ trống")]
        [StringLength(500, ErrorMessage = "So Luong Ky Tu Trong Khoang 20-500", MinimumLength = 20)]
        public String Title { set; get; }

        [Required(ErrorMessage = "Bạn không được bỏ trống")]
        [MinLength(50, ErrorMessage = "Tối thiểu 50 ký tự")]
        public String Body { set; get; }

        [Required(ErrorMessage = "Bạn không được bỏ trống")]
        [DataType(DataType.DateTime, ErrorMessage = "Bạn nhập không đúng ngày tháng")]
        public DateTime DataCreated { set; get; }

        [Required(ErrorMessage = "Bạn không được bỏ trống")]
        [DataType(DataType.DateTime, ErrorMessage = "Bạn nhập không đúng ngày tháng")]
        public DateTime DateUpdated { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }

        public int AccountID { set; get; }
        public virtual Account Account { set; get; }
    }
}