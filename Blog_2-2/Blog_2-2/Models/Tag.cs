﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2_2.Models
{
    public class Tag
    {
        public int TagID { set; get; }
        [Required(ErrorMessage = "Bạn không được bỏ trống")]
        [StringLength(100, ErrorMessage = "Nhập vào trong khoảng 10-100 ký tự", MinimumLength = 10) ]
        public String content { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
        
    }
}