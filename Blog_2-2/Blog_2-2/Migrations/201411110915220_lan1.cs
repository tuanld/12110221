namespace Blog_2_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountID = c.Int(nullable: false, identity: true),
                        Password = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        LastName = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.AccountID);
            
            AddColumn("dbo.Bai Viet", "Account_AccountID", c => c.Int());
            AlterColumn("dbo.Bai Viet", "ID", c => c.Int(nullable: false));
            AlterColumn("dbo.Bai Viet", "Title", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Bai Viet", "Body", c => c.String(nullable: false));
            AlterColumn("dbo.Comments", "Body", c => c.String(nullable: false));
            AlterColumn("dbo.Comments", "Author", c => c.String(nullable: false));
            AlterColumn("dbo.Tags", "content", c => c.String(nullable: false, maxLength: 100));
            AddForeignKey("dbo.Bai Viet", "Account_AccountID", "dbo.Accounts", "AccountID");
            CreateIndex("dbo.Bai Viet", "Account_AccountID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Bai Viet", new[] { "Account_AccountID" });
            DropForeignKey("dbo.Bai Viet", "Account_AccountID", "dbo.Accounts");
            AlterColumn("dbo.Tags", "content", c => c.String());
            AlterColumn("dbo.Comments", "Author", c => c.String());
            AlterColumn("dbo.Comments", "Body", c => c.String());
            AlterColumn("dbo.Bai Viet", "Body", c => c.String(maxLength: 250));
            AlterColumn("dbo.Bai Viet", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Bai Viet", "ID", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.Bai Viet", "Account_AccountID");
            DropTable("dbo.Accounts");
        }
    }
}
