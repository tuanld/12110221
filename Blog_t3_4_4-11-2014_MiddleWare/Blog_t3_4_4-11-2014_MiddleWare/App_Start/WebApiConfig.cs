﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Blog_t3_4_4_11_2014_MiddleWare
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
