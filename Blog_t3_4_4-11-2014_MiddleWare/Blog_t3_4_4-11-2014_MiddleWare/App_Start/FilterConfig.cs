﻿using System.Web;
using System.Web.Mvc;

namespace Blog_t3_4_4_11_2014_MiddleWare
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}