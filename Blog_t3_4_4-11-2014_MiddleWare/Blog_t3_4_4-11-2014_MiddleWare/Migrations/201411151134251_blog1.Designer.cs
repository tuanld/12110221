// <auto-generated />
namespace Blog_t3_4_4_11_2014_MiddleWare.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class blog1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(blog1));
        
        string IMigrationMetadata.Id
        {
            get { return "201411151134251_blog1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
