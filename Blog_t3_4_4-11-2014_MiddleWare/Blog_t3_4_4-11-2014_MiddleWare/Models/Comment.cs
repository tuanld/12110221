﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_t3_4_4_11_2014_MiddleWare.Models
{
    public class Comment
    {
        public int ID { set; get; }
        public String Body { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DataCreated { set; get; }


        public int LastTime
        {
            get
            {
                return (DateTime.Now - DataCreated).Minutes ;
            }
        }

        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}