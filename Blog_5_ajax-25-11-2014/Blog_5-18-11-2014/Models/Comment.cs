﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_5_2_25_11_2014.Models
{
    public class Comment
    {
        public int ID { set; get; }
        public String Body { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DataCreated { set; get; }

        public int LastTime
        {
            get
            {
                return ((DateTime.Now - DataCreated ).Minutes % 60);
            }
        }

        public int LastTimeHour
        {
            get
            {
                return (DateTime.Now - DataCreated).Hours % 24;
            }
        }

        public int LastTimeDay
        {
            get
            {
                return (DateTime.Now - DataCreated).Days ;
            }
        }

        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}