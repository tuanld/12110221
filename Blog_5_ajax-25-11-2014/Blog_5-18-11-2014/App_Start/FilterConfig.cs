﻿using System.Web;
using System.Web.Mvc;

namespace Blog_5_2_25_11_2014
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}