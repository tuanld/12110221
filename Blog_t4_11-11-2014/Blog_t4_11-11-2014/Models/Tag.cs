﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog_t4_11_11_2014.Models
{
    public class Tag
    {
        public int TagID { set; get; }
        public String content { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}