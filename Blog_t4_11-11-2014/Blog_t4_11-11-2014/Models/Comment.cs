﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_t4_11_11_2014.Models
{
    public class Comment
    {
        public int ID { set; get; }
        public String Body { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DataCreated { set; get; }

        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}