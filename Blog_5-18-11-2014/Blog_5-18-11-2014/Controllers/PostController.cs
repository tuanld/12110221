﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog_5_18_11_2014.Models;

namespace Blog_5_18_11_2014.Controllers
{
    [Authorize]
    public class PostController : Controller
    {
        private BlogDBContext db = new BlogDBContext();

        //
        // GET: /Post/
        [AllowAnonymous]
        public ActionResult Index()
        {
            var posts = db.Posts.Include(p => p.UserProFile);
            return View(posts.ToList());
        }

        //
        // GET: /Post/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewData["idpost"] = id;
            return View(post);
        }

        //
        // GET: /Post/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /Post/Create

        [HttpPost]
        public ActionResult Create(Post post,string content)
        {
            if (ModelState.IsValid)
            {
                int userID = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                    .Where(y => y.UserName == User.Identity.Name).Single().UserId;

                post.DateCreated = DateTime.Now;
                // Tao list cac Tag
                List<Tag> Tags = new List<Tag>();
                // Tach cac tag theo dau ,
                string[] TagContent = content.Split(',');
                // Lap cac tag vua tach
                foreach (string item in TagContent)
                {
                    //Tim xem Tag Content da co hay chua
                    Tag TagExits = null;
                    var ListTag = db.Tags.Where(y => y.content.Equals(item));
                    if (ListTag.Count() > 0)
                    {
                        // neu co tag roi thi them post vao 
                        TagExits = ListTag.First();
                        TagExits.Posts.Add(post);
                    }
                    else
                    {
                        // neu chua co tag thi tao moi
                        TagExits = new Tag();
                        TagExits.content = item;
                        TagExits.Posts = new List<Post>();
                        TagExits.Posts.Add(post);
                    }
                    //add vao List cac Tag
                    Tags.Add(TagExits);
                }
                //Gan list Tag cho Post
                post.Tags = Tags;

                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserId);
            return View(post);
        }

        //
        // GET: /Post/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserId);
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserId);
            return View(post);
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}